package uk.ac.cam.automation.example.stepdefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class GooglePage {
    @Given("I go to Google")
    public void iGoToGoogle() {
        Site.goToURL("http://www.google.com");
        Site.webDriverWait().until(ExpectedConditions.titleIs("Google"));
    }

    @When("I enter the search term {string}")
    public void iEnterTheSearchTerm(String term) {
        Site.enterTextBoxDetails(By.xpath("//input[@title='Search']"), term);
        Site.click(By.xpath("//input[@value='Google Search']"));
    }

    @Then("I see the link {string} on the page")
    public void iSeeTheLinkOnThePage(String link) {
        Site.webDriverWait().until(ExpectedConditions.elementToBeClickable(By.linkText(link)));
    }

    @Then("The page title is {string}")
    public void thePageTitleIs(String title) {
        Site.webDriverWait().until(ExpectedConditions.titleIs(title));
    }
}
